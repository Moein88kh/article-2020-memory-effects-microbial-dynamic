function [BB,RR,GG]=method3(mu,t0,T,h,Number,Kout,Kin,Blue,Red,Green)
global N b Kij

BB=zeros(Number,1);RR=zeros(Number,1);GG=zeros(Number,1);

for num=1:Number
tStart=tic;
    
% interaction matrix
var=.1*randn(N);
Kij=Kout*ones(N);
Kij(Blue,Blue)=Kin; Kij(Red,Red)=Kin; Kij(Green,Green)=Kin; % same coexistance
Kij=Kij+var;

b=ones(N,1)+.1*randn(N,1); % growth rate
x0=.1*rand(N,1); % initial conditions

% solver for fractional differential equation
[~, x] = FDE_PI12_PC(mu,@fun,t0,T,x0,h); 

% Extract values for the location of steady states in the triangle
G=sum(x(Green,end)); 
B=sum(x(Blue,end)); 
R=sum(x(Red,end)); 
all=G+R+B;
GG(num)=G/all;RR(num)=R/all;BB(num)=B/all;

tEnd=toc(tStart);
disp(['Computation time for sample (', num2str(num), ') is ',num2str(tEnd)])
end
end
% =========================================================================
% =========================================================================
function dx=fun(~,x)

global b N Ki

dx=zeros(N,1);

for i=1:N
dx(i)=x(i)*(b(i).*fi_Xk(i, x)-Ki(i).*x(i));
end
end
% =========================================================================
% =========================================================================
function fi=fi_Xk(i, x)
global n N Kij
fi=1;
K=1:N;K(i)=[];
for j=1:N-1
    k=K(j);
fi=fi*(Kij(i,k).^n/(Kij(i,k).^n+x(k).^n));
end
end