function [t,x,B]=method1(Perturbation)
global T N mu x0 bb b

switch Perturbation
    case 'False'
        Fun=@fun;
    %----------------------------------------------------------------------
    case 'Pulse'
        Fun=@fun1;
        % Check if the final time passes the second pulse
        if T<330
                    warning('MATLAB:FinalTimeNotProper',...
                        'The final time T=%d should be more than 330 to pass the second pulse',T);
        end
    %----------------------------------------------------------------------        
    case 'Periodic'    
        Fun=@fun2;
    %----------------------------------------------------------------------        
    case 'OUP'        
        Fun=@fun3;
        clear b        
        load('b3OUP.mat');
        bb=@(t,N)b(t,N);
        % Check if the final time is suitable with produced OUP
        if T>700
            error('MATLAB:FinalTimeNotCompatible', ...
                'The final time T=%d should be less than 700', T);
        end
    %----------------------------------------------------------------------
    case 'OUP_new'  
        b=OUP(T,N);
        bb=@(t,N)b(t,N);
        Fun=@fun3;
end
B=b;

t0=0; % initial time
h=0.01; % step size for computing


% solver for fractional differential equation
[t, x] = FDE_PI12_PC(mu,Fun,t0,T,x0,h);
end
% =========================================================================
% =========================================================================
function dx=fun(~,x)

global b N Ki

dx=zeros(N,1);

for i=1:N
dx(i)=x(i)*(b(i).*fi_Xk(i, x)-Ki(i).*x(i));
end
end
% =========================================================================
% =========================================================================
function dx=fun1(t,x)

global b N Ki

dx=zeros(N,1);

%%% pulse pertubation
b11=0.2; b12=4.5;
if t>60 && t<100
    B=b;
    B(1)=b11;
elseif t>200 && t<330
    B=b;
    B(1)=b12;
else
    B=b;
end
%%%

for i=1:N
dx(i)=x(i)*(B(i).*fi_Xk(i, x)-Ki(i).*x(i));
end
end
% =========================================================================
% =========================================================================
function dx=fun2(t,x)

global b N Ki

dx=zeros(N,1);

%%% periodic pertubation
mm=20;m=ceil(mod(t/(mm*4),mm));
b11=0.2; b12=4.5;
if t==0
    B=b;
elseif t>=mm*(4*m-3) && t<mm*(4*m-2)
    B=b;
    B(1)=b11;
    elseif t>=mm*(4*m-1) && t<mm*(4*m)
    B=b;
    B(1)=b12;
else 
        B=b;
end
%%%

for i=1:N
dx(i)=x(i)*(B(i).*fi_Xk(i, x)-Ki(i).*x(i));
end
end
% =========================================================================
% =========================================================================
function dx=fun3(t,x)
global bb  N Ki

dx=zeros(N,1);
%%% OUP pertubation
    if t==0        
    B=bb(1,1:N);        
    else
    B=bb(ceil(t),1:N);        
    end
%%%

for i=1:N
dx(i)=x(i)*(B(i).*fi_Xk(i, x)-Ki(i).*x(i));
end
end
% =========================================================================
% =========================================================================
function fi=fi_Xk(i, x)
global n N Kij
fi=1;
K=1:N;K(i)=[];
for j=1:N-1
    k=K(j);
fi=fi*(Kij(i,k).^n/(Kij(i,k).^n+x(k).^n));
end
end
% =========================================================================
% =========================================================================
function b=OUP(T,N)
% Ornstein Uhlenbeck stochastic process
% $dX_t = \theta (\mu - X_t)dt + \sigma dW_t$

%Seed for RNG
vector=1:2^20;
seed=vector(randi(length(vector)));

% OU parameters:
%mu, s0, vol, theta
phi = 1;         % Reverting level
b0 = 1;           % Initial value
Vol = .1;   % volatility of each step
theta = .08;      % Speed of mean reversion
             
% Using simple discretisation
b = OrnsteinUhlenbeck(T, N, seed, phi, b0, Vol, theta);
 end
